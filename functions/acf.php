<?php

/*
    Advanced Custom Fields
*/


// Add options pages
if(function_exists('acf_add_options_page')) {
    acf_add_options_page();
    acf_add_options_sub_page('Header');
    acf_add_options_sub_page('Footer');
}