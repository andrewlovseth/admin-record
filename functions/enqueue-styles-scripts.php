<?php

/*
	Enqueue Styles & Scripts
*/

function esa_enqueue_styles_and_scripts() {
    // Register and noConflict jQuery 3.4.1
    wp_register_script( 'jquery.3.5.1', 'https://code.jquery.com/jquery-3.5.1.min.js' );
    wp_add_inline_script( 'jquery.3.5.1', 'var jQuery = $.noConflict(true);' );

    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    // Add style.css and third-party css
    // wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/vcx3lxt.css' );
    wp_enqueue_style( 'style', get_template_directory_uri() . '/admin-record.css');

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_template_directory_uri() . '/js/plugins.js', array( 'jquery.3.5.1' ));
    wp_enqueue_script( 'custom-site', get_template_directory_uri() . '/js/site.js', array( 'jquery.3.5.1' ));
}

add_action( 'wp_enqueue_scripts', 'esa_enqueue_styles_and_scripts', 10 );