<?php

/*
	Template Name: Home
*/

get_header(); ?>

	<?php get_template_part('template-parts/homepage/intro'); ?>
	<?php get_template_part('template-parts/homepage/filter'); ?>

	<section class="documents grid">
		<?php get_template_part('template-parts/homepage/documents-categories'); ?>
		<?php get_template_part('template-parts/homepage/documents-list'); ?>
	</section>

<?php get_footer(); ?>