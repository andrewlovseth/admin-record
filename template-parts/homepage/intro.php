<?php if(get_field('intro_copy')): ?>

	<section class="intro grid">
		<div class="copy p1">
			<?php echo get_field('intro_copy'); ?>
		</div>
	</section>

<?php endif; ?>