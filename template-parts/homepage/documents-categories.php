<?php 

$google_sheet = get_field('google_sheet');
$spreadsheet_id = $google_sheet['sheet_id'];
$categories_tab_id = $google_sheet['categories_tab_id'];
$cats_slug = sanitize_title_with_dashes($categories_tab_id);

if(get_transient($cats_slug)) {
    $categories = get_transient($cats_slug);
} else {
    $categories = esa_get_sheet_data($cats_slug, $spreadsheet_id, HOUR_IN_SECONDS, $categories_tab_id);
}

?>

<div class="documents-categories">
	<div class="sticky-wrapper">
		<div class="section-header">
			<h4>Categories</h4>
		</div>

		<div class="categories-list">
			<h5>Jump to:</h5>

			<?php if(!empty($categories)): ?>
				
				<ul>
					<?php 
						foreach ($categories as $category): 
							$title = $category[0];
							$slug = sanitize_title_with_dashes($title);
					?>

						<li><a href="#<?php echo $slug; ?>" data-cat="<?php echo $slug; ?>"><?php echo $title; ?></a></li>

					<?php endforeach; ?>			
				</ul>
			<?php endif; ?>

		</div>			
	</div>
</div>