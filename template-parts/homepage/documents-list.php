<?php 

$stream_opts = [
    "ssl" => [
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ]
];

$google_sheet = get_field('google_sheet');
$spreadsheet_id = $google_sheet['sheet_id'];
$document_tab_id = $google_sheet['documents_tab_id'];
$categories_tab_id = $google_sheet['categories_tab_id'];

$sheet_slug = sanitize_title_with_dashes($document_tab_id);
$cats_slug = sanitize_title_with_dashes($categories_tab_id);


$base_url = get_field('document_base_url');
$today_date = date('Ymd');

if(get_transient($sheet_slug)) {
    $entries = get_transient($sheet_slug);
} else {
    $entries = esa_get_sheet_data($sheet_slug, $spreadsheet_id, HOUR_IN_SECONDS, $document_tab_id);
}

if(get_transient($cats_slug)) {
    $categories = get_transient($cats_slug);
} else {
    $categories = esa_get_sheet_data($cats_slug, $spreadsheet_id, HOUR_IN_SECONDS, $categories_tab_id);
}


?>

<div class="documents-list">
	<table id="documents-table">
		<?php get_template_part('template-parts/homepage/table-header'); ?>

		<?php if(!empty($categories)): ?>

			<tbody>
				<?php 
					foreach ($categories as $category): 
						$cat_title = $category[0];
						$cat_slug = sanitize_title_with_dashes($cat_title);
				?>
			 
				    <tr id="<?php echo $cat_slug; ?>" class="header-row">
				    	<td class="cat-header" colspan="5"><span><?php echo $cat_title; ?></span></td>				        
				    </tr>


					<?php
						$files = array_filter($entries, function ($entry) use ($cat_title) {
							return ($entry[0] == $cat_title); }
						);

						if(!empty($files)): ?>

							<?php
								foreach ($files as $file): 
									$date = $file[1];
									$document_title = $file[2];
									$author = $file[3];
									$doc = $file[4];
									$ext = pathinfo($doc, PATHINFO_EXTENSION);
							?>

								<tr class="file">
									<td class="cat"><?php echo $cat_title; ?></td>
									<td class="date"><?php echo $date; ?></td>
									<td class="title">
										<?php if($file !== ''): ?>
											<a href="<?php echo $base_url; ?><?php echo $doc; ?>" rel="external"><?php echo $document_title; ?></a>
										<?php else: ?>
											<?php echo $document_title; ?>
										<?php endif; ?>											
									</td>
									<td class="author"><?php echo $author; ?></td>
									<td class="file-type">
										<?php if($doc !== ''): ?>
											<a href="<?php echo $base_url; ?><?php echo $doc; ?>" rel="external"><?php echo $ext; ?></a>
										<?php endif; ?>											
									</td>									
								</tr>

							<?php endforeach; ?>

						<?php endif; ?>
				
				<?php endforeach; ?>

			</tbody>

		<?php endif; ?>

	</table>
</div>