<?php 

    $logo = get_field('site_logo', 'options'); 

    if($logo):
?>

    <div class="site-logo">
        <a href="<?php echo site_url('/'); ?>">
            <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
        </a>
    </div>

<?php endif; ?>