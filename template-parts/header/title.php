<div class="site-title">
    <h1><?php echo get_field('project_name', 'options'); ?></h1>
    <h2><?php echo get_field('site_name', 'options'); ?></h2>
</div>