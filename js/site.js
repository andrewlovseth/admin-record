(function ($, window, document, undefined) {
    $(document).ready(function ($) {
        // rel="external"
        $('a[rel="external"]').click(function () {
            window.open($(this).attr('href'));
            return false;
        });

        // SmoothScroll to document category
        $('.documents-categories a, .smooth').smoothScroll();

        // Live Filter/Search for documents with keywords
        $('#documents-filter').on('keyup', function () {
            var value = $(this).val().toLowerCase();

            $('#documents-table tr.file').filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });

            // Add filtered class to .documents-list if query value is present
            if ($(this).val().length !== 0) {
                $('.documents-list').addClass('filtered');
            } else {
                $('.documents-list').removeClass('filtered');
            }
        });

        // Disable filter button
        $('.filter button').on('click', function () {
            return false;
        });
    });
})(jQuery, window, document);
